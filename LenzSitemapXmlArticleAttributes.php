<?php
namespace LenzSitemapXmlArticleAttributes;

use Enlight_Event_EventArgs;
use Shopware\Components\Plugin;

class LenzSitemapXmlArticleAttributes extends Plugin {
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatchSecure_Frontend_SitemapXml' => 'onPostDispatchSecureFrontendSitemapXml',
        ];
    }

    public function onPostDispatchSecureFrontendSitemapXml(Enlight_Event_EventArgs $args) {
        $subject = $args->getSubject();
        $view = $subject->View();

        $sitemap = $view->sitemap;
        $articleAttributeIds = $this->getArticleAttributesIDs();
        $attributes = $this->getArticleAttributes($articleAttributeIds);

        foreach($sitemap['articles'] as $key => $val) {
            $sitemap['articles'][$key]['attributes'] = $attributes[$val['id']];
        }

        $view->sitemap = $sitemap;
    }

    private function getArticleAttributesIDs () {
        $query = Shopware()->Db()->query(
            'SELECT
              id as articleID,
              main_detail_id as mainDetailID
            FROM
              s_articles sa
              '
        );

        $articleAttributeIds = [];
        foreach ($query->fetchAll() as $val) {
            $articleAttributeIds[$val['articleID']] = $val['mainDetailID'];
        }

        return $articleAttributeIds;
    }

    private function getArticleAttributes($articleAttributeIds) {
        /** @var \Shopware\Bundle\AttributeBundle\Service\DataLoader $attributeDataLoader */
        $attributeDataLoader = $this->container->get('shopware_attribute.data_loader');

        $attributes = [];
        foreach ($articleAttributeIds as $key => $value) {
            $attributes[$key] = $attributeDataLoader->load('s_articles_attributes', $value);
        }

        return $attributes;
    }
}